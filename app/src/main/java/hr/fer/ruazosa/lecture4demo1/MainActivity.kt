package hr.fer.ruazosa.lecture4demo1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import hr.fer.ruazosa.lecture4demo1.databinding.ActivityMainBinding
import java.util.Date

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initRepository()

        val viewModel = ViewModelProvider(
            this, ViewModelProvider.AndroidViewModelFactory(application)).get(
            NotesViewModel::class.java)

        binding.notesListRecyclerViewId.layoutManager = LinearLayoutManager(applicationContext)
        val adapter = NotesAdapter(viewModel)
        binding.notesListRecyclerViewId.adapter = adapter

        val decorator = DividerItemDecoration(applicationContext, LinearLayoutManager.VERTICAL)
        decorator.setDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.divider)!!)
        binding.notesListRecyclerViewId.addItemDecoration(decorator)

        viewModel.listOfNotes.observe(this, Observer {adapter.notifyDataSetChanged()})

        binding.floatingActionButton.setOnClickListener {
            val startNoteDetailsActivity = Intent(this, NoteDetailsActivity::class.java)
            startActivity(startNoteDetailsActivity)
        }

    }

    override fun onResume() {
        super.onResume()
        binding.notesListRecyclerViewId.adapter?.notifyDataSetChanged()
    }

    private fun initRepository() {
        /*
        NoteRepository.notes.add(Note(noteDate = Date(), noteDescription = "Some description", noteTitle = "SomeTitle"))
        NoteRepository.notes.add(Note(noteDate = Date(), noteDescription = "Some description2", noteTitle = "SomeTitle2"))
        NoteRepository.notes.add(Note(noteDate = Date(), noteDescription = "Some description", noteTitle = "SomeTitle"))
        NoteRepository.notes.add(Note(noteDate = Date(), noteDescription = "Some description2", noteTitle = "SomeTitle2"))
        NoteRepository.notes.add(Note(noteDate = Date(), noteDescription = "Some description", noteTitle = "SomeTitle"))
        NoteRepository.notes.add(Note(noteDate = Date(), noteDescription = "Some description2", noteTitle = "SomeTitle2"))
        NoteRepository.notes.add(Note(noteDate = Date(), noteDescription = "Some description", noteTitle = "SomeTitle"))
        NoteRepository.notes.add(Note(noteDate = Date(), noteDescription = "Some description2", noteTitle = "SomeTitle2"))
        NoteRepository.notes.add(Note(noteDate = Date(), noteDescription = "Some description", noteTitle = "SomeTitle"))
        NoteRepository.notes.add(Note(noteDate = Date(), noteDescription = "Some description2", noteTitle = "SomeTitle2"))
        NoteRepository.notes.add(Note(noteDate = Date(), noteDescription = "Some description", noteTitle = "SomeTitle"))
        NoteRepository.notes.add(Note(noteDate = Date(), noteDescription = "Some description2", noteTitle = "SomeTitle2"))
        NoteRepository.notes.add(Note(noteDate = Date(), noteDescription = "Some description", noteTitle = "SomeTitle"))
        NoteRepository.notes.add(Note(noteDate = Date(), noteDescription = "Some description2", noteTitle = "SomeTitle2"))
        NoteRepository.notes.add(Note(noteDate = Date(), noteDescription = "Some description", noteTitle = "SomeTitle"))
        NoteRepository.notes.add(Note(noteDate = Date(), noteDescription = "Some description2", noteTitle = "SomeTitle2"))
        NoteRepository.notes.add(Note(noteDate = Date(), noteDescription = "Some description", noteTitle = "SomeTitle"))
        NoteRepository.notes.add(Note(noteDate = Date(), noteDescription = "Some description2", noteTitle = "SomeTitle2"))
        NoteRepository.notes.add(Note(noteDate = Date(), noteDescription = "Some description", noteTitle = "SomeTitle"))
        NoteRepository.notes.add(Note(noteDate = Date(), noteDescription = "Some description2", noteTitle = "SomeTitle2"))
        NoteRepository.notes.add(Note(noteDate = Date(), noteDescription = "Some description", noteTitle = "SomeTitle"))
        NoteRepository.notes.add(Note(noteDate = Date(), noteDescription = "Some description2", noteTitle = "SomeTitle2"))

         */
    }
}