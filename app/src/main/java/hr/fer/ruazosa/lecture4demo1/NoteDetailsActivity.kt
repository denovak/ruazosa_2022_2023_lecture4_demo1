package hr.fer.ruazosa.lecture4demo1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import hr.fer.ruazosa.lecture4demo1.databinding.ActivityNoteDetailsBinding
import java.util.Date

class NoteDetailsActivity : AppCompatActivity() {
    private lateinit var binding: ActivityNoteDetailsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityNoteDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val viewModel = ViewModelProvider(
            this, ViewModelProvider.AndroidViewModelFactory(application)).get(
            NotesViewModel::class.java)

        binding.saveNoteButtonId.setOnClickListener {
            val note = Note(noteDate = Date(), noteTitle = binding.noteDetailsTitleEditTextId.text.toString(),
                noteDescription = binding.noteDetailsNoteDescriptionEditTextId.toString())
            viewModel.saveNote(note)
            finish()
        }
    }
}