package hr.fer.ruazosa.lecture4demo1

import java.util.Date

data class Note(var noteDate: Date, var noteTitle: String, var noteDescription: String)